package com.example.MAID;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

//import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.Intent;
//import android.graphics.Color;
//import android.media.Image;
import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
//import android.view.LayoutInflater;
//import android.view.KeyEvent;
import android.view.View;
//import android.view.inputmethod.EditorInfo;
import android.widget.Button;
//import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected short VisibleStatus = 1;
    protected String varStringUsername;
    protected String varStringPassword;
    protected String varStringEmail;
    EditText[] editTexts = new EditText[3];
    int[] editTextsID = {R.id.EtUsername, R.id.EtPassword, R.id.EtEmail};
    Button[] buttons = new Button[2];
    int[] buttonsID = {R.id.BtnLogin, R.id.BtnReset};
    ImageButton visible;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for(char i = 0; i < editTexts.length; i++)
        {
            editTexts[i] = new EditText(this);
            editTexts[i] = findViewById(editTextsID[i]);
        }
        for(char i = 0; i < buttonsID.length; i++)
        {
            buttons[i] = new Button(this);
            buttons[i] = findViewById(buttonsID[i]);
        }
        visible = findViewById(R.id.IbVisible);
        resetVar();
        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    varStringUsername = editTexts[0].getText().toString();
                    varStringPassword = editTexts[1].getText().toString();
                    varStringEmail = editTexts[2].getText().toString();
                    if (varStringUsername.isEmpty())
                        Toast.makeText(getApplicationContext(), "Empty Username cannot be empty", Toast.LENGTH_LONG).show();
                    else if (varStringPassword.isEmpty())
                        Toast.makeText(getApplicationContext(), "Password cannot be empty", Toast.LENGTH_LONG).show();
                    else if (varStringEmail.isEmpty())
                        Toast.makeText(getApplicationContext(), "Email cannot be empty", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getApplicationContext(), "SignUp Success", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetVar();
            }
        });

        visible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (VisibleStatus == 0) {
                    visible.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_black_24dp));
                    editTexts[1].setTransformationMethod(new PasswordTransformationMethod());
                    VisibleStatus = 1;
                } else if (VisibleStatus == 1) {
                    visible.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp));
                    editTexts[1].setTransformationMethod(null);
                    VisibleStatus = 0;
                }

            }
        });
    }

    public void resetVar() {
        varStringUsername = null;
        varStringPassword = null;
        varStringEmail = null;
        VisibleStatus = 1;

        for (EditText editText : editTexts) {
            editText.setText(null);
        }
    }


    @Override
    public void onBackPressed() {


    }

}
